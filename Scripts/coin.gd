extends Area

signal coinCollected

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	rotate_y(deg2rad(3))
	


func _on_coin_body_entered(body):
	if body.name == "Gerson":
		$AnimationPlayer.play("bounce")
		$Timer.start()
		


func _on_Timer_timeout():
	emit_signal("coinCollected")
	queue_free()
